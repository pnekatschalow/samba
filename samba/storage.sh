#!/bin/bash -e

# setup Samba share
chmod a+rx ${MOUNT_DIR}
(echo ${PASSWORD}; echo ${PASSWORD}) | smbpasswd -a -s root
echo "[root]" >> $SMB_CONF_FILE
echo "    comment = Storage Root" >> $SMB_CONF_FILE
echo "    valid users = root" >> $SMB_CONF_FILE
echo "    create mask = 0777" >> $SMB_CONF_FILE
echo "    directory mask = 0777" >> $SMB_CONF_FILE
echo "    writable = yes" >> $SMB_CONF_FILE
echo "    path = ${MOUNT_DIR}" >> $SMB_CONF_FILE

# start Samba server
service samba start

while ps aux |grep smbd |grep -v grep > /dev/null; do
    sleep 1
done

echo "!!!SMBD process died!!!"
