# Samba Server integration with MySQL user database

The goal of this project is integrate Samba server with MySQL user database, so that users listed in the database can be authenticated by Samba server

## Docker Containers

There are two Docker containers, running Samba server and MySQL server.

### MySQL Server Container

This containers runs an instance of MySQL server with a simple user table. The default database user, password, and name is `appcious`. You can connect to the database from the local host with the following command:

     $ mysql -h localhost -u appcious -pappcious appcious

Samba should authenticate against user name stored in the user.owncloud_uid columne, using the password stored in user.storage_password column.

### Samba Server Container

The current samba server is configured to export using a local root account, configured with a simple password. Run the following command to mount the shared directory:

     $ mount -t cifs -o rw,username=root,password=password,uid=www-data,gid=www-data,file_mode=0750,dir_mode=0750 //127.0.0.1/root /mnt/tmp

Once Samba is integrated with MySQL, I should then be able to mount with the user account and password supplied by the database:

     $ mount -t cifs -o rw,username=U0887289858,password=secret,uid=www-data,gid=www-data,file_mode=0750,dir_mode=0750 //127.0.0.1/U0887289858 /mnt/tmp
