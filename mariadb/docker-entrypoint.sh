#!/bin/bash
set -e

if [ "${1:0:1}" = '-' ]; then
  set -- mysqld "$@"
fi

if [ "$1" = 'mysqld' ]; then
  DATADIR="$("$@" --verbose --help 2>/dev/null | awk '$1 == "datadir" { print $2; exit }')"
  echo 'Running mysql_install_db ...'
  mysql_install_db --datadir="$DATADIR"
  echo 'Finished mysql_install_db'
  set -- "$@" --init-file="/appcious.sql" --verbose
  chown -R mysql:mysql "$DATADIR"
fi

exec "$@"
